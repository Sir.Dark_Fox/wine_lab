package wine_lab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import wine_lab.controllers.MainController;

@SpringBootApplication
public class WineLabApplication {

	public static void main(String[] args) {
		SpringApplication.run(WineLabApplication.class, args);
	}
}
