package wine_lab.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import wine_lab.models.Article;
import wine_lab.repo.ArticleRepository;

import javax.annotation.security.RolesAllowed;
import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.UUID;
import java.util.stream.Collectors;

@Controller
public class ArticleController {

    @Autowired
    private ArticleRepository articleRepository;

    @GetMapping("/news")
    public String newsList(Model model) {
        var articles = articleRepository.findAll(Sort.by(Sort.Direction.DESC, "id"));
        model.addAttribute("articles", articles);

        return "news";
    }

    @GetMapping("/news/{id}")
    public String getNewsById(Model model, @PathVariable(required = false) long id) {

        var article = articleRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid article Id:" + id));

        model.addAttribute("article", article);
        return "news-detail";
    }

    @GetMapping("/news/add")
    public String addNewsForm(Model model) {
        model.addAttribute("article", new Article());

        return "news-add";
    }

    @PostMapping("/news/add")
    @PreAuthorize("hasRole('ADMIN')")
    public String add(
            Model model,
            @ModelAttribute Article article,
            BindingResult result,
            @RequestParam("file") MultipartFile file) throws IOException {

        if (result.hasErrors()) {
            return "news-add";
        }

        saveFile(article, file);
        article.setLastUpdate(LocalDateTime.now());

        articleRepository.save(article);
        model.addAttribute("article", article);

        return "news-detail";
    }

    @GetMapping("/news/edit/{id}")
    public String updateNewsForm(Model model,
                                 @PathVariable(required = false) long id) {
        var article = articleRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid article Id:" + id));

        model.addAttribute("article", article);

        return "news-edit";
    }

    @PostMapping("/news/update/{id}")
    public String update(
            Model model,
            @ModelAttribute Article article,
            @PathVariable("id") long id,
            BindingResult result,
            @RequestParam("file") MultipartFile file) throws IOException {

        if (result.hasErrors()) {
            article.setId(id);
            return "news-edit";
        }

        saveFile(article, file);
        article.setLastUpdate(LocalDateTime.now());
        articleRepository.save(article);

        model.addAttribute("article", article);

        return "redirect:/news";
    }

    @PostMapping("/news/delete/{id}")
    public String deleteNews(
            Model model,
            @PathVariable(required = false) long id)  {

        var article = articleRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid article Id:" + id));

        articleRepository.delete(article);

        return newsList(model);
    }

    private void saveFile(Article article, @RequestParam("file") MultipartFile file) throws IOException {
        if (file != null && !file.getOriginalFilename().isEmpty()) {

            String uploadPath = System.getProperty("user.dir") + "/src/main/resources/static/img";

            File uploadDir = new File(uploadPath);
            if (!uploadDir.exists()) {
                uploadDir.mkdir();
            }

            String uuidFile = UUID.randomUUID().toString();
            String resultFilename = uuidFile + "." + file.getOriginalFilename();

            file.transferTo(new File(uploadPath + "/" + resultFilename));

            article.setImgPath(resultFilename);
        }
    }
}
