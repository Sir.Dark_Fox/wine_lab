package wine_lab.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import wine_lab.repo.ArticleRepository;

import java.util.stream.Collectors;


@Controller
public class MainController {

    @Autowired
    private ArticleRepository articleRepository;

    @GetMapping("/")
    public String greeting(Model model) {
        var articles = articleRepository.findAll(Sort.by(Sort.Direction.DESC, "id"))
                .stream().limit(3).collect(Collectors.toList());

        model.addAttribute("viewName", "homeTest");
        model.addAttribute("fragmentName", "homeTest");
        model.addAttribute("name", "Visitor");
        model.addAttribute("articles", articles);

        return "home";
    }
}
