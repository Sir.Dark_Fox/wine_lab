package wine_lab.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.view.RedirectView;
import wine_lab.models.Article;
import wine_lab.models.Regulation;
import wine_lab.repo.RegulationRepository;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@CrossOrigin(origins = "*")
@Controller
public class RegulationController {

    @Autowired
    private RegulationRepository regulationRepository;

    @RequestMapping(value = {"/regulations", "/regulations/{id}"}, method = RequestMethod.GET)
    public String regulations(Model model, @PathVariable(required = false) String id) throws Exception {

        Regulation chosenReg;
        try {
            if (id != null && !id.isEmpty()) {
                chosenReg = regulationRepository.findById(Long.valueOf(id)).get();
            } else {
                chosenReg = regulationRepository.findAll().stream().findFirst().get();
            }
        } catch (Exception e) {
            chosenReg = new Regulation();
        }
        model.addAttribute("chosenReg", chosenReg);

        model.addAttribute("regulations", regulationRepository.findAll());

        return "regulations";
    }

    @GetMapping("/reg-delete/{id}")
    public String delete(Model model, @PathVariable("id") Long id) {

        Regulation reg = regulationRepository.findById(id).get();
        regulationRepository.delete(reg);

        return "redirect:/regulations";
    }

    @PostMapping("/regulations")
    public String save(Regulation regulation, Model model) {

        regulationRepository.save(regulation);

        model.addAttribute("regulation", regulation);
        return "redirect:/regulations";
    }

    @PostMapping("/img-uploads")
    public void uploadImages(MultipartFile file) throws IOException {

        if (file != null && !file.getOriginalFilename().isEmpty()) {

            String uploadPath = System.getProperty("user.dir") + "/src/main/resources/static/img/regulations/temp";

            File uploadDir = new File(uploadPath);
            if (!uploadDir.exists()) {
                uploadDir.mkdir();
            }

            String uuidFile = UUID.randomUUID().toString();
            String resultFilename = uuidFile + "." + file.getOriginalFilename();

            file.transferTo(new File(uploadPath + "/" + resultFilename));
        }
    }
}

