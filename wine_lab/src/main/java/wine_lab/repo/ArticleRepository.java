package wine_lab.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import wine_lab.models.Article;

public interface ArticleRepository extends JpaRepository<Article, Long> {
}
