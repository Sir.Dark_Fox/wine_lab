package wine_lab.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import wine_lab.models.Regulation;

public interface RegulationRepository extends JpaRepository<Regulation, Long> {
}
