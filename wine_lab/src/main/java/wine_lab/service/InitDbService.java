package wine_lab.service;

import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import wine_lab.models.User;
import wine_lab.repo.UserRepository;

@Service
public class InitDbService implements CommandLineRunner {
    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;

    public InitDbService(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void run(String... args) throws Exception {
        if (userRepository.count() > 0)
            return;
        User admin = new User("admin", passwordEncoder.encode("123"), "ADMIN", true);
        this.userRepository.save(admin);

    }
}
