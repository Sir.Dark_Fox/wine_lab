create table article (
    id bigint not null,
    text varchar(255),
    title varchar(255),
    primary key (id)
);

create table hibernate_sequence (
    next_val bigint
);

insert into hibernate_sequence values ( 1 );

create table user (
    id bigint not null,
    password varchar(255) not null,
    role varchar(255),
    username varchar(255) not null,
    primary key (id)
);

